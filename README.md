# PyCom Micro Sockets

Some examples for PyCom devices.

## Notes

- BLE is not working, but Wi-Fi approach in client and server works at around 100 samples per second.
- I strongly recommend limiting to about 10 to prevent crashes as the current implementation does.