import pycom
import machine
from network import WLAN
import time
import socket
import crypto

# settings
SSID = 'helmet'
PSK  = 'o4$eJuKE#7FxA^F7PcZ!70'
SRV  = '192.168.0.1'
PORT = 54321

# client is green (to know which is which)
pycom.heartbeat(False)
pycom.rgbled(0x000800)

# station mode
wlan = WLAN(mode=WLAN.STA)

# while on and has battery
while True:

    # connect
    print('Wi-Fi: Connecting...')
    while not wlan.isconnected():
        try:
            wlans = wlan.scan()
        except:
            pass
        if len(wlans) > 0:
            for net in wlans:
                if net.ssid == SSID:
                    try:
                        wlan.connect(net.ssid, auth=(WLAN.WPA2, PSK), timeout=5000)
                        while not wlan.isconnected():
                            machine.idle()
                        # TODO check for address in field 0
                        print('Wi-Fi: Connected!', wlan.ifconfig())
                    except:
                        pass
            time.sleep(5)

    # while Wi-Fi
    while wlan.isconnected():

        # read from sensor here
        sensorData = crypto.getrandbits(32)[0]
        print('Sending:', sensorData)

        # new socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((SRV, PORT))
        sock.sendto(sensorData.to_bytes(8, 'little'), (SRV, PORT))
        sock.close()
        time.sleep(0.1)