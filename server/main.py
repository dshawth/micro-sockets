import pycom
import socket
import _thread
from network import WLAN
import time

# settings
SSID = 'helmet'
PSK  = 'o4$eJuKE#7FxA^F7PcZ!70'
HOST = '0.0.0.0'
PORT = 54321

# server is red (to know which is which)
pycom.heartbeat(False)
pycom.rgbled(0x080000)

# ap mode settings
wlan = WLAN(mode=WLAN.STA_AP, ssid=SSID, auth=(WLAN.WPA2, PSK),
    channel=7, antenna=WLAN.INT_ANT)

# static IP, subnet, gateway, DNS
wlan.ifconfig(id=1, config=('192.168.0.1', '255.255.255.0', '0.0.0.0', '0.0.0.0'))

# Thread for handling a client
def client_thread(clientsocket,n):
    r = clientsocket.recv(16)
    if len(r) > 0:
        print("Received: {}".format(int.from_bytes(r, 'little')))
        clientsocket.close()
    time.sleep(0.1)

# Set up server socket
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serversocket.bind((HOST, PORT))

# Accept maximum of 5 connections at the same time
serversocket.listen(5)

# Unique data to send back
c = 0
while True:
    # Accept the connection of the clients
    (clientsocket, address) = serversocket.accept()
    # Start a new thread to handle the client
    _thread.start_new_thread(client_thread, (clientsocket, c))
    c = c+1
    time.sleep(0.01)