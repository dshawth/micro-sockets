import struct

# get coords from gps
coord = (43.00307, -75.97336)
print(coord)

# send gps coords
send_me = struct.pack('ff',coord[0],coord[1])
print(send_me, len(send_me)) # 8 bytes

# receive gps coords
recv_me = send_me
try:
    coord = tuple([round(a, 5) for a in struct.unpack('ff', recv_me)])
    print(coord)
except:
    print('not coords')