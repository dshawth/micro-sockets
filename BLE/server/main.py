from network import Bluetooth
import pycom
import time

pycom.heartbeat(False)
pycom.rgbled(0x080000) # server is red (to know which is which)

bluetooth = Bluetooth()
bluetooth.set_advertisement(name='server', service_uuid=b'1234567890123456')

def conn_cb (bt_o):
    events = bt_o.events()
    if events & Bluetooth.CLIENT_CONNECTED:
        print("Client connected")
    elif events & Bluetooth.CLIENT_DISCONNECTED:
        print("Client disconnected")

bluetooth.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=conn_cb)

bluetooth.advertise(True)

pushService = bluetooth.service(uuid=1234, isprimary=True)
pushChar = pushService.characteristic(uuid=5678, properties=Bluetooth.PROP_WRITE_NR)

def push_cb(char):
    events = char.events()
    print('push event triggered')
    if events & Bluetooth.CHAR_WRITE_EVENT:
        print("Write request with value = {}".format(char.value()))

pushChar.callback(trigger=(Bluetooth.CHAR_WRITE_EVENT | Bluetooth.CHAR_READ_EVENT), handler=push_cb)