from network import Bluetooth
import time
import pycom
import os

pycom.heartbeat(False)
pycom.rgbled(0x000800) # client is green (to know which is which)

bt = Bluetooth()
bt.start_scan(-1)

lastSensorData = None

while True:
    # read from sensor here

    sensorData = os.urandom(1)[0] % 5 # replace this

    if sensorData != lastSensorData:
        adv = bt.get_adv() # possible as a callback
        # TODO below might need to be a loop thru all the advs
        if adv:
            if bt.resolve_adv_data(adv.data, Bluetooth.ADV_NAME_CMPL) == 'server':
                print('sending {} to server'.format(sensorData))
                bt.stop_scan()
                pycom.rgbled(0x000008) # blue while connected
                try:
                    conn = bt.connect(adv.mac)
                    # do things (like send the data)
                    for service in conn.services():
                        for char in service.characteristics():
                            print(char.uuid(), char.properties(), Bluetooth.PROP_READ)
                            if (char.properties() & Bluetooth.PROP_WRITE):
                                print('found a write!')
                                char.write(bytes(sensorData))
                    conn.disconnect()
                except:
                    print('connect failed')
                pycom.rgbled(0x000800) # back to green while scanning
                bt.start_scan(-1)
            else:
                pass # print('some other BLE device')
        else:
            pass

    # this line is required (do not drop below 0.01) to prevent 100% utilization and freezing
    # if you need to go faster, you will need a fully event-oriented design
    time.sleep(0.5)
